package com.simple.sqldatabase.db_android_app;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

public class TestListAdapter extends ArrayAdapter<TestTable> {

    public TestListAdapter(Context context, int textViewResourceId, List<TestTable> objects) {
        super(context, textViewResourceId, objects);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TestTable user = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        }

        TextView id_text = convertView.findViewById(R.id.id_text);
        id_text.setText("" + user.d_id.get());

        TextView text = convertView.findViewById(R.id.text_);
        text.setText(user.d_text.get());

        CheckBox checkBox = convertView.findViewById(R.id.checkbox_);
        checkBox.setChecked(user.d_checked.get());

        return convertView;
    }
}