package com.simple.sqldatabase.db_android_app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;

import com.simple.sqldatabase.DBManager;

import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String TEST_DATABASE_NAME = "testdatabase.db";
    private ListView listView;
    private EditText editText;
    private CheckBox checkBox;

    private List<TestTable> tables;
    private DBManager dbManager;
    private TestListAdapter testListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupDatabase();

        setListview();
        editText = findViewById(R.id.edittext);
        checkBox = findViewById(R.id.checkbox);

        setButtonsListeners();
    }

    private void setupDatabase() {
        dbManager = new DBManager(this, TEST_DATABASE_NAME, TestTable.class);
        tables = dbManager.getAllRows(TestTable.class);
    }

    private void setListview() {
        testListAdapter = new TestListAdapter(getApplicationContext(), R.layout.list_item, tables);
        listView = findViewById(R.id.list);
//        listView.setOnItemClickListener(this);
        listView.setAdapter(testListAdapter);
        listView.setClickable(true);
    }

    private void setButtonsListeners() {
        findViewById(R.id.btnupdate).setOnClickListener(this);
        findViewById(R.id.btnadd).setOnClickListener(this);
        findViewById(R.id.btnload).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnadd:
                onButtonAddClick();
                break;
            case R.id.btnupdate:
                onButtonUpdateClick();
                break;
            case R.id.btnload:
                onButtonLoadClick();
                break;
        }
    }

    private void onButtonAddClick() {
        TestTable testTable = dbManager.createNewDAOTable(TestTable.class);
        testTable.d_text.set(editText.getText().toString());
        testTable.d_checked.set(checkBox.isChecked());
        dbManager.add(testTable);
        testListAdapter.add(testTable);
        testListAdapter.notifyDataSetChanged();
    }

    private void onButtonUpdateClick() {
        if (testListAdapter.getCount() == 0) {
            return;
        }
        TestTable testTable = testListAdapter.getItem(0);
        testTable.d_text.set(editText.getText().toString());
        testTable.d_checked.set(checkBox.isChecked());
        dbManager.update(testTable, testTable.d_id);
        tables = dbManager.getAllRows(TestTable.class);
        testListAdapter.clear();
        testListAdapter.addAll(tables);
        testListAdapter.notifyDataSetChanged();
    }

    private void onButtonLoadClick() {
        tables = dbManager.getAllRows(TestTable.class);
        testListAdapter.clear();
        testListAdapter.addAll(tables);
        testListAdapter.notifyDataSetChanged();
    }
}
