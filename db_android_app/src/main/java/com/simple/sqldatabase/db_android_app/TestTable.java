package com.simple.sqldatabase.db_android_app;

import com.simple.sqldatabase.DBDAOTableBase;
import com.simple.sqldatabase.annotations.DBColumn;
import com.simple.sqldatabase.fields.DBFieldBoolean;
import com.simple.sqldatabase.fields.DBFieldInteger;
import com.simple.sqldatabase.fields.DBFieldString;

public class TestTable extends DBDAOTableBase {

    public static final String TABLE_ITEM = "test_table";
    @DBColumn(primaryKey = true, autoIncrement = true)
    DBFieldInteger d_id;

    @DBColumn
    DBFieldString d_text;

    @DBColumn
    DBFieldBoolean d_checked;

    @Override
    public String getTableName() {
        return TABLE_ITEM;
    }
}
